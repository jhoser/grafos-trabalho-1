#ifndef _GRAFO_H
#define _GRAFO_H

#include <stdio.h>

//global
  int **m;

//------------------------------------------------------------------------------
// (apontador para) estrutura de dados para representar um vértice
// 
// o vértice tem um nome, que é uma "string"

typedef struct vertice {
  char name[1024];
  int index;
  struct vertice *neighbors;
  struct vertice *next; 
} *vertice;

//------------------------------------------------------------------------------
// (apontador para) estrutura de dados para representar um grafo
// 
// o grafo tem um nome, que é uma "string"
typedef struct grafo {
  int n_v;
  struct vertice *v_list;
} *grafo;

//------------------------------------------------------------------------------
// desaloca toda a memória usada em *g
// 
// devolve 1 em caso de sucesso,
//         ou 
//         0, caso contrário

int destroi_grafo(grafo);

//------------------------------------------------------------------------------
// lê um grafo de input.
// um grafo é um arquivo onde cada linha tem zero, uma ou duas
// strings (sequência de caracteres sem "whitespace").
// 
// devolve o grafo lido. Caso o arquivo esteja mal formado o
// comportamento da função é indefinido

grafo le_grafo(FILE *);  

//------------------------------------------------------------------------------
// escreve o grafo g em output, no mesmo formato que o usado por le_grafo()
//
// devolve o grafo escrito,
//         ou 
//         NULL, em caso de erro 

grafo escreve_grafo(FILE *, grafo);

//------------------------------------------------------------------------------
// devolve o coeficiente de agrupamento de g
// ou seja, o número de tríades fechadas divido pelo 
// total de tríades (ou seja, a soma de tríades abertas e fechadas).
// ou 0 se não há triades no grafo

double coeficiente_agrupamento_grafo(grafo );

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// recebe um rotulo e varre o grafo em busca de um vertice
// de mesmo rotulo caso encontrado retorna o indice
// caso o contrario retorna -1 para indicar que 
// o grafo não possui vertice com tal rotulo

vertice is_in_graph(grafo ,char *);
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// recebe o indice de dois vertices no grafo e cria uma aresta os ligando
void edge_link(vertice,vertice);
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//Cria uma matriz de adjacencia apartir do grafo lido
void Adj_M(grafo );
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//Inicializa um vertice novo no grafo
vertice vertice_create(grafo, int , char *);
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//faz a copia do rotulo,grau e indice de um vertice 
vertice v_copy(vertice ,vertice);
//------------------------------------------------------------------------------

#endif
