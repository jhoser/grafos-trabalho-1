#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "grafo.h"

#define it_isnt NULL

vertice is_in_graph (grafo g, char *s){
	vertice x;
	x = g->v_list;
	while(x){
   		if(strcmp(x->name,s) == 0){
   			return x;
   		}
   		x = x->next;
    }
   return it_isnt;
}

vertice vertice_create(grafo g , int index, char *buffer){
	vertice x;
  
    x = malloc (sizeof(struct vertice));
    x->index = index;
    strcpy(x->name,buffer);
    x->next = g->v_list;
    g->v_list = x;

    return x;
   
}

vertice v_copy(vertice x ,vertice v){
    strcpy(x->name,v->name);
    x->index = v->index;
    x->next = NULL;
  
    return x;
}

void Adj_M(grafo g){
	int i ;
	vertice x,y;
	//aloca matriz e vertices auxiliares
	x = malloc(sizeof(vertice));
	y = malloc(sizeof(vertice));

	m= (int**) calloc ((unsigned)g->n_v , sizeof(int*));
	for( i = 0; i < g->n_v ; ++i)
		m[i] = (int *) calloc ((unsigned)g->n_v , sizeof(int));

	//preenche a matriz
	x = g->v_list;
	while(x){
		y = x->neighbors;
		while(y){
			m[x->index][y->index] = 1;
			m[y->index][x->index] = 1;
			y = y->next; 
		}
		x = x->next;
	}
	
}

void edge_link(vertice index1,vertice index2){
	vertice u;

	u = malloc(sizeof(struct vertice));
	
	v_copy(u,index1);
	
	u->next = index2->neighbors;
	index2->neighbors = u;

}

grafo le_grafo(FILE *input) {
	int i , j ;
   	char aux;
   	char name[2][1024];
   	vertice index1, index2;

   	grafo g;

  
   	g = malloc (sizeof(grafo));//aloca g
   	g->n_v = 0;//inicializa numero de vertices


   	while( (aux = (char)fgetc(input)) != EOF ){//enquanto não leu EOF
      	for( i = 0 ; (i < 2) && (aux != '\n') && (aux != EOF); ++i ){//enquanto não leu 2 rotulos ou '\n' ou EOF
	 		if(aux == ' ')//se leu ' '
	    		aux = (char)fgetc(input);//le novamente
	    		
	 			for( j = 0 ; (aux != ' ') && (aux != '\n' ) && 
	 				(aux != EOF); ++j, aux = (char)fgetc(input)){//enquanto não leu ' ' ou '\n' ou EOF
	    			
	    			name[i][j] = aux;//preenche rotulo
	 			}
	 			name[i][j] = '\0';//indica fim do rotulo
       	}
		if(i == 2){//se leu 2 rotulos
	  		if((index1 = is_in_graph( g, name[0] )) == it_isnt ){//se rotulo não esta no grafo
	   			index1 = vertice_create(g, g->n_v, name[0] );//inicializa vertice com rotulo e indice
	   			g->n_v++;//aumenta numero de vertices
	  		}
	  		if((index2 = is_in_graph( g, name[1] )) == it_isnt ){//se rotulo não esta no grafo
	   			index2 = vertice_create( g , g->n_v, name[1] );//inicializa vertice com rotulo e indice
	   			g->n_v++;//aumenta numero de vertices
        	}
        	if(index1 != index2){
			   edge_link(index1, index2);//cria aresta direcionada v-u
         }
	  	}

	  	if(i == 1){//se foi lido um unico rotulo
			if((index1 = is_in_graph( g, name[0] )) == it_isnt ){//se rotulo não esta no grafo
		  		vertice_create(g, g->n_v , name[0]);//inicializa vertice com rotulo e indice
		  		g->n_v++;//aumenta numero de vertices
	   		}
	    }
    }

	Adj_M(g);//cria matriz de adjacencia de g
    
    return g;
}

int destroi_grafo(grafo g){
	vertice x,trash;
	if(!g)//se g é vazio
		return -1; //aborta
		
	x = g->v_list;
   	while(x){
   		while(x->neighbors){
   			trash = x->neighbors;
   			if(trash){
   				x->neighbors = trash->neighbors;
   			}
   			else {
   				x->neighbors = NULL;
   			}
   			free(trash);   			
   		}
   		trash = x->next;
   		if(trash){
   			x->next= trash->next;
   		}
   		else {
   			x->next = NULL;
   		}
   		free(trash);
   		x = x->next;
    }

    free(x);
    free(g);
   
   return 1;
}

grafo escreve_grafo(FILE *output,grafo g){
   	vertice x,y;

   	if(!g){// se g é vazio
   		return NULL;//aborta
   	}
	
	x = malloc(sizeof(vertice));
	y = malloc(sizeof(vertice));

   	x = g->v_list;
	while(x){
		y = x->neighbors;
		while(y){
			fprintf(output,"%s %s\n",x->name,y->name );
			y = y->next; 
		}
		x = x->next;
	}
	return g;
}

double coeficiente_agrupamento_grafo(grafo g){
	int i, j, k, open, close;
	double c;
	 
	open = close = 0;
	 
   for(i = 0 ; i < g->n_v ; ++i){  // encontrou u
		for(j = 0 ; j < g->n_v ; j++)
			if(m[i][j] == 1){  // encontrou u-v
				for(k = j+1 ; k < g->n_v ; ++k  )
					if(m[i][k] == 1){ // encontrou u-v-w
						if((m[j][k] == 1) && (m[k][j] == 1)){ // se v e vizinho de w
							close++;//e fechado
						}else{//se não
							open++;//é aberto
						}
					}			
			}
	}

	if((open == 0) && (close == 0)){
		c = 0;
	} else {
		c = close;
		c/=(open+close);
	}
	return c;
}
